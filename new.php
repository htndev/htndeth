<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Мое объявление</title>
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<link rel="stylesheet" href="css/_normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/new.css">
	<link rel="icon" href="img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="scripts/search.js"></script>
</head>
<body>
<div class="grid">
	<?php require_once "includes/header.php" ?>
	<main>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
		<div class="main-section">
			<h1>Анкета</h1>
			<form action="includes/mail.php" method="post" enctype="multipart/form-data">
				<div class="field">
					<input type="text" name="first_name" id="first_name" placeholder="Александр" required>
					<label for="first_name">Имя</label>
				</div>
				<div class="field">
					<input type="text" name="last_name" id="last_name" placeholder="Ванильчук" required>
					<label for="last_name">Фамилия</label>
				</div>
				<div class="field">
					<input type="text" name="patronymic" id="patr" placeholder="Владимирович">
					<label for="patr">Отчество</label>
				</div>
				<div class="field">
					<input type="email" name="user_email" id="email" placeholder="example@example.com">
					<label for="email">Email</label>
				</div>
				<div class="field">
					<input type="text" name="user_title" id="title" placeholder="Велосипед СОЮЗ" required>
					<label for="title">Название объявления</label>
				</div>
				<div class="field">
					<input type="text" name="price" id="price" placeholder="Цена" required>
					<label for="price">Цена</label>
				</div>
				<div class="field">
					<input type="tel" name="phone" id="phone" placeholder="0961234567" required>
					<label for="phone">Телефон</label>
				</div>
				<div class="field">
					<input type="text" name="town" id="town" placeholder="Житомир" required>
					<label for="town">Город, село, итп.</label>
				</div>
				<div class="field">
					<select name="category">
						<option value="">Категория</option>
						<option value="недвижемость">Недвижемость</option>
						<option value="транспорт">Транспорт</option>
						<option value="растения">Растения</option>
						<option value="электроника">Электроника</option>
						<option value="работа">Работа</option>
						<option value="животные">Животные</option>
						<option value="отдам%20даром">Отдам даром</option>
						<option value="личные%20вещи">Личные вещи</option>
						<option value="для%20детей">Для детей</option>
						<option value="мебель">Мебель</option>
						<option value="другое">Другое</option>
					</select>
				</div>
				<div class="field">
					<input type="file" name="picture" id="picture" accept=".png,.jpg,.jpeg">
					<label for="picture">Прикрепите фото</label>
				</div>
				<div class="comment">
					<h3>Напишите описание</h3>
					<textarea name="user_description"></textarea>
				</div>
				<button type="submit">Отправить</button>
			</form>
		</div>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
	</main>
	<?php require_once "includes/footer.php" ?>
</div>
<script>
  function initMap() {
	var myLatLng = {
	  lat: 50.244725,
	  lng: 28.637313
	};

	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom  : 15,
	  center: myLatLng
	});

	var marker = new google.maps.Marker({
	  position: myLatLng,
	  map     : map,
	  title   : 'Наш офис'
	});
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu44J4ZCNIZ2DmkkSNLDddgmq1YGF-V54&callback=initMap" async defer></script>
</body>
</html>