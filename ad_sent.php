<?php
	require "includes/cfg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Заявка отправлена</title>
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<link rel="stylesheet" href="css/_normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/mail_sent.css">
	<link rel="icon" href="img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="scripts/search.js"></script>
	<script src="scripts/redirect.js"></script>
</head>
<body>
<div class="grid">
	<?php require_once "includes/header.php"?>
	<main>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
		<div class="main-section">
			<h2>Ваша заявка отправлена на рассмотрение!</h2>
			<p>Вы будете перенаправлены на главную страницу через <span class="num">5</span>...</p>
		</div>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
	</main>
	<?php require_once "includes/footer.php"?>
</div>
</body>
</html>