<?php
	require_once "includes/cfg.php";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title><?php echo $cfg['title']?></title>
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<link rel="stylesheet" href="css/_normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/index_page.css">
	<link rel="icon" href="img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="scripts/search.js"></script>
</head>
<body>
	<div class="grid">
		<?php require_once "includes/header.php" ?>
		<main>
			<aside>
    		<div class="ad">
        	<p>Здесь могла быть Ваша реклама</p>
    		</div>
			</aside>
			<div class="main-section">
				<h1>Доска объявлений</h1>
				<h2>Выберите необходимую Вам категорию</h2>
				<div class="categories">
					<div class="category">
						<a href="ads.php?c=Недвижемость">
							<i class="fas fa-home"></i>
							<h3>Недвижимость</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Транспорт">
							<i class="fas fa-car"></i>
							<h3>Транспорт</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Растения">
							<i class="fas fa-leaf"></i>
							<h3>Растения</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Электроника">
							<i class="fas fa-desktop"></i>
							<h3>Электроника</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Работа">
							<i class="fas fa-briefcase"></i>
							<h3>Работа</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Животные">
							<i class="fas fa-paw"></i>
							<h3>Животные</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Отдам радом">
							<i class="fas fa-hand-holding"></i>
							<h3>Отдам радом</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Личные вещи">
							<i class="fas fa-box-open"></i>
							<h3>Личные вещи</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Для детей">
							<i class="fas fa-child"></i>
							<h3>Для детей</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Мебель">
							<i class="fas fa-couch"></i>
							<h3>Мебель</h3>
						</a>
					</div>
					<div class="category">
						<a href="ads.php?c=Другое">
							<i class="fas fa-ellipsis-h"></i>
							<h3>Другое</h3>
						</a>
					</div>
				</div>
				<div class="view-more">
					<a href="ads.php">
						<div title="Все объявления">Еще</div>
					</a>
				</div>
			</div>
			<aside>
    		<div class="ad">
        	<p>Здесь могла быть Ваша реклама</p>
    		</div>
			</aside>
			<div></div>
			<div class="info">
				<h2>Про htndeth</h2>
				<p>HTNDETH - бесплатная доска объявления Украины, на которой можно продать б/у или новые товары, разместить объявление о предложении частных услуг и найти новые знакомства в своем городе или по всей Украине.</p>
				<h3>Почему именно htndeth?</h3>
				<p>Размещение объявлений бесплатно</p>
				<p>Каждый пользователь HTNDETH может подать на бесплатной основе до 100 объявлений в бесплатные рубрики. В разделы «Услуги» и «Работа» вы можете разместить бесплатно объявления согласно квот для платных рубрик.</p>
				<p>Высокая популярность доски объявлений</p>
				<p>Ежедневно HTNDETH посещают множество пользователей, которые на сайте находят необходимые вещи или услуги, размещают новые объявления о продаже товаров, аренде квартир, а также о желании найти новые знакомства.</p>
				<p>Все пользователи доски объявлений HTNDETH могут для увеличения эффективности своих объявлений, использовать бесплатные обновления вверх списка рубрики, выделить объявления цветом или воспользоваться топ-зоной.</p>
				<h3>Как подать объявление?</h3>
				<p>Внизу сайта Вы найдете в форму, заполните ее и отправьте. Вам придет ответ в течении 2-3 рабочих дней.</p>
				<h3>Партнеры</h3>
				<div class="partners">
					<a href="https://www.visa.com.ua/ru_UA" target="_blank" title="Visa"><i class="fab fa-cc-visa"></i></a>
					<a href="https://www.mastercard.ru/ru-ru.html" target="_blank" title="Mastercard"><i class="fab fa-cc-mastercard"></i></a>
					<a href="https://www.microsoft.com/uk-ua/" target="_blank" title="Microsoft"><i class="fab fa-microsoft"></i></a>
					<a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter" title="Twitter"></i></a>
					<a href="https://www.amazon.com/" target="_blank" title="Amazon"><i class="fab fa-amazon"></i></i></a>
					<a href="https://www.google.com/" target="_blank"><i class="fab fa-google" title="Google"></i></a>
				</div>
			</div>
		</main>
		<?php require_once "includes/footer.php" ?>
	</div>
	<script>
		function initMap()
    {
      var myLatLng = {
        lat: 50.244725,
        lng: 28.637313
      };

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom:   15,
        center: myLatLng
      });

      var marker = new google.maps.Marker({
                                            position: myLatLng,
                                            map:      map,
                                            title:    'Наш офис'
                                          });
    }
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu44J4ZCNIZ2DmkkSNLDddgmq1YGF-V54&callback=initMap" async defer></script>
</body>
</html>