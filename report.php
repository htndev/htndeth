<?php
require "includes/cfg.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Жалоба</title>
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<link rel="stylesheet" href="css/_normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/report.css">
	<link rel="icon" href="img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="scripts/search.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
				integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
</head>
<body>
<div class="grid">
	<?php require_once "includes/header.php" ?>
	<main>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
		<div class="main-section">
			<h1>Жалоба</h1>
		<?php
		$query = "SELECT `title`, `name`, `surname` , `patronymic` FROM `ads` WHERE `id` = '" . $_GET['id'] . "'";
		$ads   = mysqli_query($connection, $query);
		if ( mysqli_num_rows($ads) <= 0 ) {
			?>
					<h2>К сожалению, на такую статью невозможно отправить жалобу.</h2>
			<?php
		}
		else {
			$ad = mysqli_fetch_assoc($ads);
			?>
					<h2>На объявление "<?php echo $ad['title']; ?>"</h2>
					<p>Владелец объявления: <?php echo $ad['surname'] . ' ' . $ad['name'] . ' ' . $ad['patronymic'] ?></p>
			<?php
		}
		?>
			<form action="includes/report_upload.php" method="post" enctype="multipart/form-data" name="report">
				<div class="form-group">
					<label for="exampleFormControlSelect1">Причина</label>
					<select class="form-control" id="exampleFormControlSelect1" required name="type">
						<option>Мошенничество</option>
						<option>Оскорбления</option>
						<option>Грубое поведение</option>
						<option>Не соответствует указаному</option>
						<option>Другое</option>
					</select>
					<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
					<input type="hidden" name="title" value="<?php echo $ad['title']; ?>">
				</div>
				<div class="form-group">
					<label for="exampleFormControlTextarea1">Комментарий</label>
					<textarea class="form-control" id="exampleFormControlTextarea1" placeholder="(обязательно)" name="comment"
										required></textarea>
				</div>
				<button type="submit" class="btn btn-danger">Отправить</button>
			</form>
		</div>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
	</main>
	<?php require_once "includes/footer.php" ?>
	<script>
	function initMap() {
	  var myLatLng = {
		lat: 50.244725,
		lng: 28.637313
	  };

	  var map = new google.maps.Map(document.getElementById('map'), {
		zoom  : 15,
		center: myLatLng
	  });

	  var marker = new google.maps.Marker({
		position: myLatLng,
		map     : map,
		title   : 'Наш офис'
	  });
	}
	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu44J4ZCNIZ2DmkkSNLDddgmq1YGF-V54&callback=initMap"
					async
					defer></script>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
					integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
					crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
					integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
					crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
					integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
					crossorigin="anonymous"></script>
</div>
</body>
</html>