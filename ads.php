<?php
require_once "includes/cfg.php";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>
	  <?php
	  if ( $_GET['c'] == "" ) {
		  echo "Объявления";
	  }
	  else {
		  echo str_replace('%20', ' ', ucfirst($_GET['c']));
	  }
	  ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<link rel="stylesheet" href="css/_normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/ads.css">
	<link rel="icon" href="img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="scripts/search.js"></script>
</head>
<body>
<div class="grid">
	<?php require_once "includes/header.php" ?>
	<main>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
		<div class="main-section">
			<div class="top-section">
				<h1>Объявления</h1>
		  <?php require_once "includes/search_panel.php" ?>
				<div class="ads">
					<div class="cat">
							<span>Все в категории: <strong><?php
					  if ( $_GET['c'] == "" ) {
						  echo "Все";
					  }
					  else {
						  echo str_replace('%20', ' ', ucfirst($_GET['c']));
					  } ?>
								</strong></span>
						<span>Найдено результатов: <strong><?php
					if ( ( !$_GET['c'] && !$_GET['t'] ) || ( $_GET['c'] == "" && $_GET['s'] == "" && $_GET['t'] == "" ) ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads`";
					}

					if ( $_GET['c'] != "" && $_GET['s'] == "" && $_GET['t'] == "" ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads` WHERE `category` = " . "'" . str_replace('%20', ' ', ucfirst($_GET['c'])) . "'" . "";
					}

					if ( $_GET['c'] == "" && $_GET['s'] == "" && $_GET['t'] != "" ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads` WHERE `town` = " . "'" . $_GET['t'] . "'" . "";
					}

					if ( $_GET['c'] == "" && $_GET['s'] != "" && $_GET['t'] == "" ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads` WHERE `title` LIKE " . "'%" . strtolower($_GET['s']) . "%'" . "";
					}

					if ( $_GET['c'] != "" && $_GET['s'] != "" && $_GET['t'] == "" ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads` WHERE `category` = " . "'" . str_replace('%20', ' ', ucfirst($_GET['c'])) . "'" . " AND `title` LIKE " . "'%" . $_GET['s'] . "%'" . "";
					}

					if ( $_GET['c'] == "" && $_GET['s'] != "" && $_GET['t'] != "" ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads` WHERE `town` = " . "'" . $_GET['t'] . "'" . " AND `title` LIKE " . "'%" . $_GET['s'] . "%'" . "";
					}

					if ( $_GET['c'] != "" && $_GET['s'] != "" && $_GET['t'] != "" ) {
						$query = "SELECT `id`, `title`, `img`, `town`, `date`, `category` FROM `ads` WHERE `category` = " . "'" . str_replace('%20', ' ', ucfirst($_GET['c'])) . "'" . " AND `title` LIKE " . "'%" . $_GET['s'] . "%'" . "AND `town` = " . "'" . $_GET['t'] . "'" . "";
					}
					?>
					<?php
					$ads  = mysqli_query($connection, $query);
					$rows = mysqli_num_rows($ads);
					if ( $rows <= 0 ) {
						echo 'К сожалению, ничего не найдено.';
					}
					else {
						echo $rows;
					}
					?></strong></span>
						<div class="aligner">
							<div class="new">
								<a href="new.php">Мое объявление</a>
							</div>
						</div>
					</div>
					<div class="ads-wrapper">
			  <?php
			  while ( $ad = mysqli_fetch_assoc($ads) ) { ?>
								<div class="ad">
									<a href="ad.php?id=<?php echo $ad['id']; ?>"><img src="img/uploads/<?php echo $ad['img']; ?>"
																																		alt="<?php echo $ad['title']; ?>"></a>
									<div class="info">
										<a href="ad.php?id=<?php echo $ad['id']; ?>"><h3><?php echo $ad['title']; ?></h3></a>
										<div class="cat">
											<div class="tag">
												<div class="icon"><i class="fas fa-tags"></i></div>
												<div class="tags"><a href="?с=<?php echo $ad['category']; ?>"><?php echo $ad['category']; ?></a>
												</div>
											</div>
											<div class="geo">
												<div class="icon"><i class="fas fa-map-marked"></i></div>
												<div class="city"><a href="?t=<?php echo $ad['town']; ?>"><?php echo $ad['town']; ?></a></div>
											</div>
											<div class="date">
												<div class="icon"><i class="fas fa-clock"></i></div>
												<div class="time"><?php echo $ad['date']; ?></div>
											</div>
										</div>
									</div>
								</div>
				  <?php
			  }
			  ?>
					</div>
				</div>
			</div>
		</div>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
	</main>
	<?php require_once "includes/footer.php" ?>
</div>
<script>
  function initMap() {
	var myLatLng = {
	  lat: 50.244725,
	  lng: 28.637313
	};

	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom  : 15,
	  center: myLatLng
	});

	var marker = new google.maps.Marker({
	  position: myLatLng,
	  map     : map,
	  title   : 'Наш офис'
	});
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu44J4ZCNIZ2DmkkSNLDddgmq1YGF-V54&callback=initMap" async
				defer></script>
</body>
</html>