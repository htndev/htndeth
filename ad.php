<?php
require_once "includes/cfg.php";
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title><?php echo $cfg['title'] ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=0">
	<link rel="stylesheet" href="css/_normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/ad.css">
	<link rel="icon" href="img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="scripts/search.js"></script>
</head>
<body>
<div class="grid">
	<?php require_once "includes/header.php" ?>
	<main>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
		<div class="main-section">
			<div class="top-section">
				<h2>Поиск</h2>
		  <?php require_once "includes/search_panel.php" ?>
			</div>
		<?php
		$query = "SELECT `name`, `surname`, `patronymic`, `title`, `img`, `price`, `phone`, `town`, `category`, `description`, `date` FROM `ads` WHERE `id` = " . "'" . $_GET['id'] . "'";
		?>
			<div class="product">
		  <?php
		  $ads = mysqli_query($connection, $query);
		  if ( mysqli_num_rows($ads) <= 0 ) {
			  ?>
						<div class="first-line">
							<div class="title">
								<h3>Объявление не найдено</h3>
							</div>
						</div>
			  <?php
		  }
		  else {
			  $ad = mysqli_fetch_assoc($ads);
			  ?>
						<div class="first-line">
							<div class="title">
								<h3><?php echo $ad['title'] ?></h3>
							</div>
						</div>
						<div class="second-line">
							<img src="img/uploads/<?php echo $ad['img']; ?>" alt="<?php echo $ad['title'] ?>">
							<div class="quick-info">
								<div class="top">
									<h4>&#36; <?php echo $ad['price']; ?></h4>
									<div class="call-block">
										<a href="tel:+380966020340="><i class="fas fa-phone"></i><?php echo $ad['phone']; ?></a>
									</div>
									<p><?php echo $ad['surname']; ?> <?php echo $ad['name']; ?> <?php echo $ad['patronymic']; ?></p>
								</div>
								<div class="bottom">
									<div class="report">
										<a href="report.php?id=<?php echo $_GET['id']?>"><i class="fas fa-flag"></i>Пожаловаться</a>
									</div>
								</div>
							</div>
						</div>
						<div class="third-line">
							<div class="heading">
								<div class="wrapper">
									<div class="data">
										<i class="fas fa-map-marked"></i>
										<a href="ads.php?t=<?php echo $ad['town']; ?>"><?php echo $ad['town']; ?></a>
									</div>
									<div class="data">
										<i class="fas fa-tags"></i>
										<a href="ads.php?c=<?php echo $ad['category']; ?>"><?php echo $ad['category']; ?></a>
									</div>
									<div class="data">
										<i class="fas fa-clock"></i>
										<p><?php
						echo $ad['date']
						?></p>
									</div>
								</div>
								<div class="description">
									<h3>Описание</h3>
									<p><?php echo $ad['description']; ?></p>
								</div>
							</div>
						</div>
			  <?php
		  }
		  ?>
			</div>
		</div>
		<aside>
			<div class="ad">
				<p>Здесь могла быть Ваша реклама</p>
			</div>
		</aside>
	</main>
	<?php require_once "includes/footer.php" ?>
</div>
<script>
  function initMap() {
	var myLatLng = {
	  lat: 50.244725,
	  lng: 28.637313
	};

	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom  : 15,
	  center: myLatLng
	});

	var marker = new google.maps.Marker({
	  position: myLatLng,
	  map     : map,
	  title   : 'Наш офис'
	});
  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu44J4ZCNIZ2DmkkSNLDddgmq1YGF-V54&callback=initMap" async
				defer></script>
</body>
</html>