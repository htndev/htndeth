<footer>
	<div class="about">
		<h2>Мы на карте</h2>
		<div id="map"></div>
		<h3><strong>Адрес:</strong> 10005, Украина, г. Житомир, ул. Чудновская, 103</h3>
	</div>
	<div class="contact-us">
		<div class="wrapper">
			<h2>Htndeth – доска объявлений.</h2>
			<p>Все права защищены и соблюдены. &#169; htndeth, 2018-<?php echo date("Y")?></p>
			<h3>Следите за нами в соц. сетях:</h3>
			<div class="soc-networks">
				<a href="http://t.me/htnpro" title="Vanilchuk's telegram" target="_blank"><i class="fab fa-telegram-plane"></i></a>
				<a href="http://t.me/Dethrider" title="Kluiev's telegram" target="_blank"><i class="fab fa-telegram-plane"></i></a>
				<a href="https://www.instagram.com/htnpro/" target="_blank" title="Vanilchuk's instagram"><i class="fab fa-instagram"></i></a>
				<a href="https://www.instagram.com/dethr1der/" target="_blank" title="Kluiev's instagram"><i class="fab fa-instagram"></i></a>
				<a href="https://www.facebook.com/alexandr.heathen" target="_blank" title="Vanilchuk's facebook"><i class="fab fa-facebook-f"></i></a>
				<a href="https://www.facebook.com/anatolii.kliuiev" target="_blank" title="Kluiev's facebook"><i class="fab fa-facebook-f"></i></a>
			</div>
		</div>
	</div>
</footer>