<?php
require "cfg.php";
$file   = $_FILES['picture'];
$upload = "../img/uploads/";
if ( isset($file) ) {
	$errors    = [];
	$file_name = $_FILES['picture']['name'];
	$file_size = $_FILES['picture']['size'];
	$file_tmp  = $_FILES['picture']['tmp_name'];
	$file_type = $_FILES['picture']['type'];
	$file_ext  = strtolower(end(explode('.', $_FILES['picture']['name'])));
	if ( $file_size > 2097152 ) {
		$errors[] = 'Файл должен быть 2Мб';
	}
	if ( empty($errors) == true ) {
		$file_wo_symbols = str_replace(' ', '', str_replace(',', '', $file_name));
		$file_loc        = $upload . preg_replace("/\.[^.]+$/", "", $file_wo_symbols) . '-' . rand(1, 9999) . '.' . $file_ext;
		move_uploaded_file($file_tmp, $file_loc);
	}
}

require_once( 'phpmailer/PHPMailerAutoload.php' );
$mail          = new PHPMailer;
$mail->CharSet = 'utf-8';

$full_name = [
	'first_name' => $_POST['first_name'],
	'last_name'  => $_POST['last_name'],
	'patronymic' => $_POST['patronymic']
];

$email = $_POST['user_email'];
$title = $_POST['user_title'];
$phone = $_POST['phone'];
$town  = $_POST['town'];
$price = $_POST['price'];
$cat   = $_POST['category'];
$desc  = $_POST['user_description'];

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host       = $cfg['mail']['host'];                 // Specify main and backup SMTP servers
$mail->SMTPAuth   = true;                             // Enable SMTP authentication
$mail->Username   = $cfg['mail']['mail'];              // Ваш логин от почты с которой будут отправляться письма
$mail->Password   = $cfg['mail']['pass'];                      // Ваш пароль от почты с которой будут отправляться письма
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port       = 465;                              // TCP port to connect to / этот порт может отличаться у других провайдеров

$mail->setFrom($cfg['mail']['mail']); // от кого будет уходить письмо?
$mail->addAddress($cfg['mail']['mail']);     // Кому будет уходить письмо
$mail->addAttachment($file_loc);         // Add attachments
$mail->isHTML(true);                                  // Set email format to HTML


$mail->Subject = 'Объявление ' . $title;
$mail->Body    =
	'<b style="font-size: 2em; color: #718093;">Заявка на модерацию объявления</b><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Название объявления: </b><i style="font-size: 1.5em; color: #40739e;">' . $title . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Цена: </b><i style="font-size: 1.5em; color: #40739e;">$' . $price . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">ФИО: </b><i style="font-size: 1.5em; color: #40739e;">' . ucfirst($full_name['last_name']) . ' ' . ucfirst($full_name['first_name']) . ' ' . ucfirst($full_name['patronymic']) . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Категория: </b><i style="font-size: 1.5em; color: #40739e;">' . ucfirst(str_replace('_', ' ', $cat)) . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Email: </b><i style="font-size: 1.5em; color: #40739e;">' . $email . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Телефон: </b><i style="font-size: 1.5em; color: #40739e;">' . $phone . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Город: </b><i style="font-size: 1.5em; color: #40739e;">' . $town . '</i><br>
	 <b style="font-size: 1.5em; color: #2f3640;">Описание: </b><i style="font-size: 1.5em; color: #40739e;">' . $desc . '</i><br>
	';
$mail->AltBody = '';

if ( !$mail->send() ) {
	echo '<b>Обновите страницу и подтвердите повторение запроса.</b>';
}
else {
	header('location: ../ad_sent.php');
}
?>