<?php
require "cfg.php";
require_once( 'phpmailer/PHPMailerAutoload.php' );
$mail = new PHPMailer;

$mail->CharSet = 'utf-8';
$mail->isSMTP();
$mail->Host       = $cfg['mail']['host'];
$mail->SMTPAuth   = true;
$mail->Username   = $cfg['mail']['mail'];
$mail->Password   = $cfg['mail']['pass'];
$mail->SMTPSecure = 'ssl';
$mail->Port       = 465;

$mail->setFrom($cfg['mail']['mail']);
$mail->addAddress($cfg['mod']['mail']);
$mail->isHTML(true);

$mail->Subject = 'Жалоба на объявление ' . $_POST['title'];
$mail->Body    = '<h1>Ифнормация о жалобе</h1><br><h3>Адрес объявления: </h3><i>htn.pro/ad.php?id=' . $_POST['id'] . '</i><br><h3>Тип жалобы:</h3><i>' . $_POST['type'] . '</i><br><h3>Комментарий: </h3><i>' . $_POST['comment'];
$mail->AltBody = '';

if ( !$mail->send() ) {
	echo '<b>Обновите страницу и подтвердите повторение запроса.</b>';
}
else {
	header('location: ../ad_sent.php');
}
?>