<div class="search-panel">
	<form action="/ads.php" method="get">
		<select name="c">
			<option value="">Категория</option>
			<option value="Недвижемость">Недвижемость</option>
			<option value="Транспорт">Транспорт</option>
			<option value="Растения">Растения</option>
			<option value="Электроника">Электроника</option>
			<option value="Работа">Работа</option>
			<option value="Животные">Животные</option>
			<option value="Отдам%20даром">Отдам даром</option>
			<option value="Личные%20вещи">Личные вещи</option>
			<option value="Для%20детей">Для детей</option>
			<option value="Мебель">Мебель</option>
			<option value="Другое">Другое</option>
		</select>
		<input type="search" name="s" placeholder="Искать объявление...">
		<select name="t">
			<option value="">Город</option>
			<option value="Житомир">Житомир</option>
			<option value="Чернигов">Чернигов</option>
			<option value="Харьков">Харьков</option>
			<option value="Запорожье">Запорожье</option>
			<option value="Николаев">Николаев</option>
			<option value="Мариуполь">Мариуполь</option>
			<option value="Винница">Винница</option>
			<option value="Херсон">Херсон</option>
			<option value="Полтава">Полтава</option>
			<option value="Черкассы">Черкассы</option>
			<option value="Черновцы">Черновцы</option>
			<option value="Ровно">Ровно</option>
			<option value="Тернополь">Тернополь</option>
			<option value="Луцк">Луцк</option>
			<option value="Ужгород">Ужгород</option>
			<option value="Киев">Киев</option>
			<option value="Днепр">Днепр</option>
			<option value="Одесса">Одесса</option>
			<option value="Львов">Львов</option>
			<option value="Суммы">Суммы</option>
		</select>
		<input type="submit" value="Искать">
	</form>
</div>