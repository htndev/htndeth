<header>
	<div class="full">
		<div class="logo"><a href="/">htndeth</a></div>
		<form action="/ads.php" method="get">
			<div class="search-container">
				<div class="search">
					<div>
						<input type="search" placeholder="Что ищем?" required name="s">
					</div>
				</div>
			</div>
		</form>
		<div class="header-panel">
			<div class="btn draw-border">
				<a href="#"><i class="fas fa-user"></i></a>
			</div>
			<div class="btn draw-border">
				<a href="#"><i class="fas fa-user-plus"></i></a>
			</div>
			<div class="btn draw-border">
				<a href="new.php"><i class="fas fa-envelope"></i></a>
			</div>
		</div>
	</div>
	<div class="mobile">
		<div class="first-line">
			<div class="logo"><a href="/">htndeth</a></div>
			<div class="search-btn"><i class="fas fa-bars"></i></div>
		</div>
		<div class="search-mob">
			<h3>Поиск</h3>
			<form action="/ads.php" method="get">
				<input type="search" placeholder="Что ищем?">
				<input type="submit" value="Искать">
			</form>
			<div class="header-panel">
				<a href="#">Вход</a>
				<a href="#">Регистрация</a>
				<a href="new.php">Мое объявление</a>
			</div>
			<div class="close">
				<i class="fas fa-times"></i>
			</div>
		</div>
	</div>
</header>