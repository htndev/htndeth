<?php
require "cfg.php";
require_once( 'phpmailer/PHPMailerAutoload.php' );
$mysqli = new mysqli($cfg['db']['server'], $cfg['db']['username'], $cfg['db']['password'], $cfg['db']['db_name']);
// PHP MAIL
$mail          = new PHPMailer;
$mail->CharSet = 'utf-8';

if ( isset($_POST["submit"]) ) {
	$name        = htmlspecialchars($_POST['name']);
	$surname     = htmlspecialchars($_POST['surname']);
	$patronymic  = htmlspecialchars($_POST['patronymic']);
	$title       = htmlspecialchars($_POST['title']);
	$img         = htmlspecialchars($_POST['img']);
	$cost        = htmlspecialchars($_POST['cost']);
	$phone       = htmlspecialchars($_POST['phone']);
	$email       = htmlspecialchars($_POST['email']);
	$town        = htmlspecialchars($_POST['town']);
	$category    = htmlspecialchars($_POST['category']);
	$description = htmlspecialchars($_POST['description']);
	if ( $name && $surname && $title && $cost && $phone && $town && $category && $description ) {
		$description = str_replace("\n,\r", "<br>", $description);
		$category    = str_replace('%20', ' ', ucfirst($category));
		if ( $email != "" ) {
			$mail->isSMTP();
			$mail->Host       = $cfg['mail']['host'];
			$mail->SMTPAuth   = true;
			$mail->Username   = $cfg['mail']['mail'];
			$mail->Password   = $cfg['mail']['pass'];
			$mail->SMTPSecure = 'ssl';
			$mail->Port       = 465;

			$mail->setFrom($cfg['mail']['mail']);
			$mail->addAddress($email);
			$mail->isHTML(true);

			$mail->Subject = "Публикация объявления " . $title;
			$mail->Body    = 'Уважаемый <b>' . $name . ' ' . $patronymic . ' ' . $surname . '</b>, Ваше объявление ' . $title . ' успешно опубликовано!';
			$mail->AltBody = '';

			if ( !$mail->send() ) {
				echo '<b>Обновите страницу и подтвердите повторение запроса.</b>';
			}
		}

		$mysqli->query("INSERT INTO ads (name, surname, patronymic, title, img, price, phone, town, category, description) VALUES ('$name', '$surname', '$patronymic', '$title', '$img', '$cost', '$phone', '$town', '$category', '$description');");
		$mysqli->close();
		header("Location: mod.php");
		exit;
	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
				content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
				integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="../css/mod.css">
	<link rel="stylesheet" href="../css/_normalize.css">
	<link rel="stylesheet" href="../css/main.css">
	<link rel="icon" href="../img/logo.ico">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
				integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<title>Moderator</title>
</head>
<body>
<div class="grid">
	<?php
	require_once "header.php";
	?>
	<h1>Добавить объявление</h1>
	<form method="post" name="add_ad" type="mod.php">
		<div>Имя <input type="text" class="form-control" placeholder="Имя" name="name" required></div>
		<div>Фамилия <input type="text" class="form-control" placeholder="Фамилия" name="surname" required></div>
		<div>Отчество <input type="text" class="form-control" placeholder="Отчество" name="patronymic"></div>
		<div>Название <input type="text" class="form-control" placeholder="Название" name="title" required></div>
		<div>Название картинки <input type="text" class="form-control" placeholder="Название картинки.jpg" name="img" required></div>
		<div>Цена <input type="text" class="form-control" placeholder="100" name="cost" required></div>
		<div>Телефон <input type="tel" class="form-control" placeholder="0961234567" name="phone" required></div>
		<div>Email <input type="email" class="form-control" placeholder="example@example.com" name="email" required></div>
		<div>Город <input type="text" class="form-control" placeholder="Город" name="town" required></div>
		<div>Категория <select class="form-control" name="category" required>
				<option value="">Категория</option>
				<option value="Недвижемость">Недвижемость</option>
				<option value="Транспорт">Транспорт</option>
				<option value="Растения">Растения</option>
				<option value="Электроника">Электроника</option>
				<option value="Работа">Работа</option>
				<option value="Животные">Животные</option>
				<option value="Отдам даром">Отдам даром</option>
				<option value="Личные вещи">Личные вещи</option>
				<option value="Для детей">Для детей</option>
				<option value="Мебель">Мебель</option>
				<option value="Другое">Другое</option>
			</select></div>
		<div>Описание <textarea placeholder="Описание товара..." name="description" class="form-control" required></textarea></div>
		<div><input type="submit" name="submit" class="form-control submit" value="Отправить"></div>
	</form>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
					integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
					crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
					integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
					crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
					integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
					crossorigin="anonymous"></script>
</body>
</html>
