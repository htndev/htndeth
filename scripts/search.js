let doc = document;
doc.addEventListener('DOMContentLoaded', function()
{
  let searchBtn   = doc.querySelector('.search-btn'),
      searchPanel = doc.querySelector('.search-mob'),
      closeBtn    = doc.querySelector('.close');
  searchBtn.addEventListener('click', function()
  {
    searchPanel.style.display = 'block';
  });
  closeBtn.addEventListener('click', function()
  {
    searchPanel.style.display = 'none';
  })
});