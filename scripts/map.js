let doc = document;
doc.addEventListener('DOMContentLoaded', function()
{
  function initMap()
  {
    var myLatLng = {
      lat: 50.244725,
      lng: 28.637313
    };

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom:   14,
      center: myLatLng
    });

    var marker = new google.maps.Marker({
                                          position: myLatLng,
                                          map:      map,
                                          title:    'Наш офис'
                                        });
  }
});