doc.addEventListener('DOMContentLoaded', function () {
  let num      = doc.querySelector('.num'),
	  value    = 5,
	  interval = 5000;
  let cust = setInterval(function () {
	setTimeout(function () {
	  clearInterval(cust);
	  location.replace('/');
	}, 5000);
	num.innerHTML = value;
	value--;
  }, 1000);
});